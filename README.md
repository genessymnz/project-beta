# CarCar

Team:

* Genessy Munoz - Service/Inventory Microservice
* Sean Burch - Sales/Inventory Microservice

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone https://gitlab.com/genessymnz/project-beta

3. Build and run the project using Docker with these commands:
```
docker volume create two-shot-pgdata
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

![Img](CarCar.png)

## Design

CarCar is comprised of three microservices.

- **Inventory**
- **Service**
- **Sales**

## Integration - How we put the "team" in "team"

Team Genesy and Team Sean worked hard to create systems that work entirely independently to create sales, services, and show inventory. Team Genessy created **Service** along with **Inventory** microservices, allowing the creation and management of services along with with tracking Inventory. Team Sean created **Sales** along with **Inventory** microservices, allowing the creation and management of sales, along with tracking Inventory.

In order to ensure that each microservice could function without interrupting the other, the **Service** and **Sales** microservices each used their own **poller** to get data from the **Inventory** microservice. These pollers ensure that all data is kept up to date for all facets of this project.

## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

## Inventory:

| Action | Method |	URL |
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/ |
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer |	GET | http://localhost:8100/api/manufacturers/:id/ |
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/ |

JSON body to send data:

Create and Update a Manufacturer (SEND THIS JSON BODY):
```
{
  "name": "Chrysler"
}
```
The return value of creating, viewing, updating a single Manufacturer:
```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
```
Getting a list of Manufacturers return value:
```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

## Vehicle Models:

| Action | Method |	URL |
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/ |
| Create a vehicle model | POST | http://localhost:8100/api/models/ |
| Get a specific vehicle model | GET | http://localhost:8100/api/models/:id/ |
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/:id/ |
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/:id/ |

JSON body to send data:

Create a Vehicle Model (SEND THIS JSON BODY):
```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```
Updating a Vehicle Model (Not possible to update manufacturer):
```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```
Getting detail of, or return value from creating or updating Vehicle Model:
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
Getting a list of Vehicle Models return value:
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

## Automobiles:


| Action | Method | URL |
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/ |
| Create an automobile | POST | http://localhost:8100/api/automobiles/ |
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/:vin/ |
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/:vin/ |
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/ |

Creating an Automobile (SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
Get detail of Automobile by VIN (ex. http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/):
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}
```
You can update the color, year, and sold status of an automobile:
```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```
Getting a list of Automobiles:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
  ]
}
```


## Service microservice

The Service microservice has 3 models: **AutomobileVO**, **Technician**, **Appointment**.

The **AutomobileVO** (Value Object) has the following properties:
- vin

The **Technician** model has the following properties:
- first_name
- last_name
- employee_id

The **Appointment** has the following properties:
- date_time
- reason
- status
- vin
- customer
- technician (A foreign key to the Technician)

## Services API Calls:

| Action | Method | URL |
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/ |
| Create a technician | POST | http://localhost:8080/api/technicians/ |
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/ |
| List appointments | GET | http://localhost:8080/api/appointments/ |
| Delete an appointment| DELETE| http://localhost:8080/api/appointments/:id/ |
| Set appointment status to "canceled"| POST | http://localhost:8080/api/appointments/:id/cancel/ |
| Set appointment status to "finished"| PUT | http://localhost:8080/api/appointments/:id/finish/ |

Getting a list of Technicians:
```
{
	"technicians": [
		{
			"first_name": "Ronald",
			"last_name": "McDonald",
			"employee_id": "1789163"
		},
		{
			"first_name": "Gojo",
			"last_name": "Satoru",
			"employee_id": "165165"
		},
		{
			"first_name": "Cody",
			"last_name": "Ko",
			"employee_id": "5791346"
		},
		{
			"first_name": "Pauly",
			"last_name": "D",
			"employee_id": "578687"
		},
		{
			"first_name": "Robert",
			"last_name": "Downey",
			"employee_id": "56156"
		},
		{
			"first_name": "Pablo",
			"last_name": "Escobar",
			"employee_id": "942579"
		}
	]
}
```

Creating a Technician (SEND THIS JSON BODY):
```
{
  "first_name": "Barbie",
  "last_name": "Ken",
  "employee_id": 754854
}
```

Delete a specific technician by ID ex(http://localhost:8080/api/technicians/3/):
```
{
"deleted": true
}
```
List appointments:
```
{
	"appointments": [
		{
			"date_time": "2024-03-06T12:05:14+00:00",
			"reason": "Flat tire",
			"status": "cancelled",
			"vin": "NJCJWS8392",
			"customer": "Billy Bob",
			"technician": {
				"first_name": "Ronald",
				"last_name": "McDonald",
				"employee_id": "1789163"
			}
		}
	]
}
```

Creating a Appointment (SEND THIS JSON BODY):
```
{
  "date_time": "2024-06-15",
  "reason": "Life is in shambles",
  "status": "Incomplete",
	"vin": "jdw463",
	"customer":"Bambie",
	"technician": 578687
}
```

Delete appointment by ID (http://localhost:8080/api/appointments/1/):
```
{
"deleted": true
}
```

Set appointment status to "canceled" by VIN #: (http://localhost:8080/api/appointments/NJCJWS8392/cancel/):
```
{
	"date_time": "2024-03-06T12:05:14+00:00",
	"reason": "Flat tire",
	"status": "cancelled",
	"vin": "NJCJWS8392",
	"customer": "Billy Bob",
	"technician": {
		"first_name": "Ronald",
		"last_name": "McDonald",
		"employee_id": "1789163"
	}
}
```

Set appointment status to "finished" by VIN #: (http://localhost:8080/api/appointments/NJCJWS8392/finish/):
```
{
	"date_time": "2024-03-06T12:05:14+00:00",
	"reason": "Flat tire",
	"status": "finished",
	"vin": "NJCJWS8392",
	"customer": "Billy Bob",
	"technician": {
		"first_name": "Ronald",
		"last_name": "McDonald",
		"employee_id": "1789163"
	}
}
```

## Sales microservice

The Sales microservice has 4 models: **AutomobileVO**, **Salesperson**, **Customer**, and **Sale**.

The **AutomobileVO** (Value Object) is populated by the **poller** and has a "vin" as one of its properties, and "sold" for other to provide clarity in testing purposes. The vin is used by the **Sale** model to keep track of whether or not a vehicle has been sold.

The **Salesperson** model has the following properties:
- first_name
- last_name
- employee_id

The **Customer** model has the following properties:
- first_name
- last_name
- address
- phone_number

The **Sale** model has the following properties:
- automobile (A foreign key to the AutomobileVO)
- salesperson (A foreign key to the Salesperson)
- customer (A foreign key to the Customer)
- price
## Service API Calls:



## Sales API Calls:

| Action | Method |	URL |
| ----------- | ----------- | ----------- |
| List salespeople| GET | http://localhost:8090/api/salespeople/ |
| Create a salesperson | POST |	http://localhost:8090/api/salespeople/ |
| Delete a specific salesperson | DELETE | http://localhost:8090/api/salespeople/:id/ |
| List customers | GET | http://localhost:8090/api/customers/ |
| Create a customer | POST | http://localhost:8090/api/customers/ |
| Delete a specific customer | DELETE |	http://localhost:8090/api/customers/:id/ |
| List sales | GET | http://localhost:8090/api/sales/ |
| Create a sale | POST | http://localhost:8090/api/sales/ |
| Delete a sale | DELETE | http://localhost:8090/api/sales/:id |

Getting a list of Salespeople:
```
{
	"salesperson": [
		{
			"first_name": "James",
			"last_name": "Bond",
			"employee_id": "nine",
			"id": 4
		},
		{
			"first_name": "Stan",
			"last_name": "Lee",
			"employee_id": "three",
			"id": 5
		}
    ]
}
```
Creating a Salesperson (SEND THIS JSON BODY):
```
{
	"first_name": "Brice",
	"last_name": "Luis",
	"employee_id": "4"
}
```
Delete a Salesperson by ID ex(http://localhost:8090/api/salespeople/9/):
{
	"deleted": true
}
```
List Customers:
{
	"customer": [
		{
			"first_name": "Merry",
			"last_name": "Poppins",
			"address": "fifty",
			"phone_number": "5128194515",
			"id": 6
		},
		{
			"first_name": "Frodo",
			"last_name": "Baggins",
			"address": "Shire",
			"phone_number": "5128194515",
			"id": 7
		},
		{
			"first_name": "Aaron",
			"last_name": "Narrow",
			"address": "Here",
			"phone_number": "5128194515",
			"id": 8
		}
	]
}
```
Create Customers (SEND THIS JSON BODY):
```
{
	"first_name": "Aaron",
	"last_name": "Narrow",
	"address": "Here",
	"phone_number": "5128194515"
}
```
Delete a Customer ex(http://localhost:8090/api/customers/:7/):
```
{
	"deleted": true
}
```
List Sales:
```
{
	"sale": [
		{
			"customer": {
				"first_name": "Merry",
				"last_name": "Poppins",
				"address": "fifty",
				"phone_number": "5128194515",
				"id": 6
			},
			"automobile": {
				"vin": "2C3CDXHG0EH195598",
				"sold": true,
				"id": 14
			},
			"salesperson": {
				"first_name": "James",
				"last_name": "Bond",
				"employee_id": "nine",
				"id": 4
			},
			"price": 10,
			"id": 24
		}
    ]
}
```
Create a Sale (SEND THIS JSON BODY):
```
{
	"automobile": "1C3CC5FB2AN120174",
	"salesperson": "1",
	"customer": "1",
	"price": "3000"
}
```
Delete a Sale ex(http://localhost:8090/api/sales/6):
```
{
	"deleted": true
}
```
