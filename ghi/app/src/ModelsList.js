import { useEffect, useState } from 'react'


function ModelsList() {

    const [models, setModels] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/models/')
        if (response.ok) {
            const { models } = await response.json()
            setModels(models)
        } else {
            console.error('Error fetching data')
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return(
        <div>
            <h1>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models?.map(model => {
                        return(
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url} style={{width: "100px", height: "60px"}} /></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ModelsList
