import React, { useEffect, useState } from "react"

function ServiceHistoryList() {
	const [appointments, setAppointments] = useState("")
    const [vin, setVin] = useState("")
	const [autos, setAutomobiles] = useState("")

	const fetchData = async () => {
		const url = "http://localhost:8080/api/appointments/"
		const automobileUrl = "http://localhost:8100/api/automobiles/"
		const response = await fetch(url)
		const automobileResponse = await fetch(automobileUrl)

		if (response.ok && automobileResponse.ok) {
			const data = await response.json()
			const automobileData = await automobileResponse.json()
			setAppointments(data.appointments)
			setAutomobiles(automobileData.autos)
		}
	}
	useEffect(() => {
		fetchData()
	}, [])

    function isVIP(vin) {
		const vip = autos.map((auto) => auto.vin)

		if (vip.includes(vin)) {
			const value = "Yes"
			return value
		} else {
			const value = "No"
			return value
		}
	}

	if (!appointments) {
		return null
	}

	const handleVinChange = (event) => {
		const value = event.target.value
		setVin(value)
	}

	if (!appointments) {
		return null
	}

	const handleSubmit = (event) => {
        event.preventDefault()
        const data = {}
        data["input"] = vin

        let vinAppointments = appointments.filter(
            (appointment => appointment.vin === vin)
        )
        setAppointments(vinAppointments)
    }
    return (
        <div>
            <h1>Service History</h1>
            <div class="mt-3">
            <table className='table table-striped'>
                <form
                    onSubmit={handleSubmit}
                    id="vin search">
                    <input
                        onChange={handleVinChange}
                        type="text"
                        placeholder="vin"
                        id="vin"
                        className="vin"
                        value={vin}
                    />
                    <button className="btn btn-primary">Submit</button>
                </form>
            <thead>
            <tr>
                <th>Vin</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>status</th>
            </tr>
            </thead>
            <tbody>
                {appointments.map((appointment)=> {
                    return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{isVIP(appointment.vin)}</td>
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                            <td>{appointment.technician.employee_id}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>


                            </tr>
						)
					})}
				</tbody>
			</table>
		</div>
        </div>
	)
}

export default ServiceHistoryList
