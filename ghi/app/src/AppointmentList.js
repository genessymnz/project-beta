import React, { useEffect, useState } from "react"

function AppointmentList() {
	const [appointments, setAppointments] = useState("")
	const [autos, setAutomobiles] = useState("")






	const fetchData = async () => {
		const url = "http://localhost:8080/api/appointments/"
		const automobileUrl = "http://localhost:8100/api/automobiles/"
		const response = await fetch(url)
		const automobileResponse = await fetch(automobileUrl)


		if (response.ok && automobileResponse.ok) {
			const data = await response.json()
			const automobileData = await automobileResponse.json()

			setAppointments(data.appointments.filter(appt => appt.status === "Incomplete"))

			setAutomobiles(automobileData.autos)
		}
	}
	useEffect(() => {
		fetchData()
	}, [])

	if (!appointments) {
		return null
	}

	function isVIP(vin) {
		const vip = autos.map((auto) => auto.vin)

		if (vip.includes(vin)) {
			const value = "Yes"
			return value
		} else {
			const value = "No"
			return value
		}
	}

	async function handleCancel(vin) {
		const response = await fetch(
			`http://localhost:8080/api/appointments/${vin}/cancel/`,
			{
				method: "PUT",
			}
		)

		if (response.ok) {
			fetchData()
            setAppointments(prev => prev.filter(appointment => appointment.vin != vin))


		}
	}

	async function handleFinish(vin) {
		const response = await fetch(
			`http://localhost:8080/api/appointments/${vin}/finish/`,
			{
				method: "PUT",
			}
		)

		if (response.ok) {
			fetchData()
            setAppointments(prev => prev.filter(appointment => appointment.vin != vin))

		}
	}


    return (
        <div>
            <h1>Service Appointments</h1>
            <div class="mt-3">
            <table className='table table-striped'>
                <thead>
                <tr>
                    <th>Vin</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>

                </tr>
                </thead>
                <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr key={appointment.vin}>
                            <td>{appointment.vin}</td>
                            <td>{isVIP(appointment.vin)}</td>
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                            <td>{appointment.technician.employee_id}</td>
                            <td>{appointment.reason}</td>

                            <td>
                            <button type="button" className="btn btn-danger" onClick={() => {handleCancel(appointment.vin)}}>
                                Cancel
                            </button>
                        </td>
                        <td>
                            <button type="button" className='btn btn-success' onClick={() => handleFinish(appointment.vin)}>
                            Finished
                            </button>
                        </td>
                        </tr>
                    )
                    })}
                </tbody>
            </table>
            </div>
    </div>
  )
}
export default AppointmentList
