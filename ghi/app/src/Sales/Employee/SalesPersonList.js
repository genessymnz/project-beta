import { useEffect, useState } from 'react'

function SalesPersonList() {
    const [salesperson, setSalesPerson] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/')
        if (response.ok) {
            const { salesperson } = await response.json()
            setSalesPerson(salesperson)
        }else {
            console.error('An error occured fetching the data')
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <h1>Salespeople</h1>
            <div className="row">
                <div className="offset-0 col-12">
                    <div className="shadow p-4 mt-4">
                        <div>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {salesperson.map((salesperson) => {
                                        return (
                                            <tr key={salesperson.id}>
                                                <td>{salesperson.employee_id}</td>
                                                <td>{salesperson.first_name}</td>
                                                <td>{salesperson.last_name}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SalesPersonList
