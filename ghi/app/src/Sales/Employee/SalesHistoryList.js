import { useEffect, useState }  from 'react'

function SalesPersonHistory() {
    const [salesperson, setSalesPerson] = useState([])
    const [sale, setSale] = useState([])
    const [select, setSelect] = useState("")

    const handleSelect = async (event) => {
        const value = event.target.value
        setSelect(value)
    }

    const getSalesPerson = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/')
        if (response.ok) {
            const data = await response.json()
            setSalesPerson(data.salesperson)
        }
    }

    const getSale = async () => {
        const response = await fetch('http://localhost:8090/api/sales/')
        if (response.ok) {
            const data = await response.json()
            setSale(data.sale)
        }
    }

    useEffect(() => {
        getSalesPerson()
        getSale()
        setSelect()
    }, [])

    return(
        <div className='row'>
            <div className='mb-3'>
                <select onChange={handleSelect} value={select} required name='sales_person' id='sales_person' className='form-select'>
                    <option value=''>Choose sales person</option>
                    {salesperson.map(salesperson => {
                        return(
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            {salesperson.first_name}
                        </option>);
                    })}
                </select>
            </div>
            <table className='table'>
                <thead>
                    <tr>
                        <th>Sales Person First Name</th>
                        <th>Sales Person Last Name</th>
                        <th>Customer First Name</th>
                        <th>Customer Last Name</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sale.map(salespeople => {
                        if (select !== "" && salespeople.salesperson.employee_id === select){
                            return (
                                <tr key={salespeople.id}>
                                    <td>{salespeople.salesperson.first_name}</td>
                                    <td>{salespeople.salesperson.last_name}</td>
                                    <td>{salespeople.customer.first_name}</td>
                                    <td>{salespeople.customer.last_name}</td>
                                    <td>{salespeople.automobile.vin}</td>
                                    <td>{salespeople.price}</td>
                                </tr>
                            )
                        }
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalesPersonHistory
