import React, { useState } from 'react'

function SalesPersonForm() {

    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = `http://localhost:8090/api/salespeople/`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        console.log(response)
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            })
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="create_salesperson">
                        <div className="text-center mb-3">
                            <span className="fs-3">Add a Salesperson</span>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.first_name} name="first_name" id="first_name" placeholder="First name" />
                            <label htmlFor="first_name">First name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.last_name} name="last_name" id="last_name" placeholder="Last name" />
                            <label htmlFor="last_name">Last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.employee_id} name="employee_id" id="employee_id" placeholder="Employee ID" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SalesPersonForm
