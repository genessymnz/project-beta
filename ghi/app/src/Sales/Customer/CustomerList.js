import { useState, useEffect } from 'react'

function CustomerList() {
    const [customer, setCustomer] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8090/api/customers/')
        if (response.ok) {
            const { customer } = await response.json()
            setCustomer(customer)
        }else {
            console.error('An error occured fetching the data')
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <h1>Customers</h1>
            <div className="row">
                <div className="offset-0 col-12">
                    <div className="shadow p-4 mt-4">
                        <div>
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Address</th>
                                        <th>Phone Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {customer.map((customer) => {
                                        return (
                                            <tr key={customer.id}>
                                                <td>{customer.first_name}</td>
                                                <td>{customer.last_name}</td>
                                                <td>{customer.address}</td>
                                                <td>{customer.phone_number}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CustomerList
