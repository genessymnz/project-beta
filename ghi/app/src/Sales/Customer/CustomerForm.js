import React, { useEffect, useState } from 'react'

function CustomerForm() {

    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = `http://localhost:8090/api/customers/`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            })
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name

        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    useEffect(() => {

    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="create_customer">
                        <div className="text-center mb-3">
                            <span className="fs-3">Add a Customer</span>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.first_name} name="first_name" id="first_name" placeholder="First name" />
                            <label htmlFor="first_name">First name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.last_name} name="last_name" id="last_name" placeholder="Last name" />
                            <label htmlFor="last_name">Last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.address} name="address" id="address" placeholder="Address" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input type="text" onChange={handleFormChange} className="form-control" value={formData.phone_number} name="phone_number" id="phone_number" placeholder="Phone number" />
                            <label htmlFor="phone_number">Phone number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CustomerForm
