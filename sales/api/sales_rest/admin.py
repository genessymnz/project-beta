from django.contrib import admin
from .models import AutomobileVO, Sale, Salesperson, Customer


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = [
        "vin",
        "sold",
    ]


@admin.register(Sale)
class Sale(admin.ModelAdmin):
    list_display = [
        "automobile",
        "salesperson",
        "customer",
        "price",
    ]


@admin.register(Salesperson)
class Salesperson(admin.ModelAdmin):
    list_display = [
        "first_name",
        "last_name",
        "employee_id",
    ]


@admin.register(Customer)
class Customer(admin.ModelAdmin):
    list_display = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]
