from django.urls import path
from .views import (salesperson_list,
                    customer_list,
                    sale_list,
                    customer_detail,
                    salesperson_detail,
                    sale_detail)


urlpatterns = [
    path("customers/", customer_list, name="customers"),
    path("customers/<int:pk>/", customer_detail, name="customers_list"),
    path("salespeople/", salesperson_list, name="salespeople"),
    path("salespeople/<int:pk>/", salesperson_detail, name="salespeople_list"),
    path("sales/", sale_list, name="sales"),
    path("sales/<int:pk>/", sale_detail, name="sales_list")
]
